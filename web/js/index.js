const data = []
let currentQuestion = 1

function init(data, nbQuestion) {
  const { questions } = data
  const currentQuestion = questions[nbQuestion]
  const reponses = questions[nbQuestion].reponses
  $('.imageQ').attr('src', `img/q${nbQuestion}.png`)
  $('.r1').text(reponses[0].question)
  $('.r1').css('background-color', reponses[0].color)
  $('.r1').attr('id', reponses[0].couleurResultat)
  $('.r2').text(reponses[1].question)
  $('.r2').css('background-color', reponses[1].color)
  $('.r2').attr('id', reponses[1].couleurResultat)
  $('.r3').text(reponses[2].question)
  $('.r3').css('background-color', reponses[2].color)
  $('.r3').attr('id', reponses[2].couleurResultat)
  $('.r4').text(reponses[3].question)
  $('.r4').css('background-color', reponses[3].color)
  $('.r4').attr('id', reponses[3].couleurResultat)
}

$('document').ready(function() {
  $.getJSON('model.json', function(json) {
    data.push(json[0])
    init(data[0], currentQuestion)
  })
  $('.startQ').click(function(event) {
    event.preventDefault()
    if (
      $('#lastname').val() !== '' &&
      $('#firstname').val() &&
      $('#email').val() &&
      $('.cgu').prop('checked') === true
    ) {
      data[0].user.email = $('#lastname').val()
      data[0].user.nom = $('#firstname').val()
      data[0].user.prenom = $('#email').val()
      data[0].user.formation = $('#past').val()
      data[0].user.telephone = $('#phone').val()
      $('.formulaire').hide()
      $('.questionnaire').removeAttr('hidden')
    }
  })
  $('.reponse').click(function() {
    currentQuestion++
    if (currentQuestion <= 5) {
      data[0].resultat[$(this).attr('id')]++
      init(data[0], currentQuestion)
    } else {
      $('.questionnaire').hide()
      $('.resultatFinal').removeAttr('hidden')
      drawRes(data[0].resultat)
      const max = Math.max(
        data[0].resultat.bleu,
        data[0].resultat.jaune,
        data[0].resultat.rouge,
        data[0].resultat.vert
      )
      switch (max) {
        case data[0].resultat.bleu:
          $('#couleur1').text(' Bleu')
          data[0].resultat.bleu = 0
          data[0].user.couleur1 = 'bleu'
          break
        case data[0].resultat.jaune:
          $('#couleur1').text(' Jaune')
          data[0].resultat.jaune = 0
          data[0].user.couleur1 = 'jaune'
          break
        case data[0].resultat.rouge:
          $('#couleur1').text(' Rouge')
          data[0].resultat.rouge = 0
          data[0].user.couleur1 = 'rouge'
          break
        default:
          $('#couleur1').text(' Vert')
          data[0].resultat.vert = 0
          data[0].user.couleur1 = 'vert'
          break
      }
      const max2 = Math.max(
        data[0].resultat.bleu,
        data[0].resultat.jaune,
        data[0].resultat.rouge,
        data[0].resultat.vert
      )
      switch (max2) {
        case data[0].resultat.bleu:
          data[0].user.couleur2 = 'bleu'
          break
        case data[0].resultat.jaune:
          data[0].user.couleur2 = 'jaune'
          break
        case data[0].resultat.rouge:
          data[0].user.couleur2 = 'rouge'
          break
        default:
          data[0].user.couleur2 = 'vert'
          break
      }
      $.getJSON('res.json', function(json) {
        $('.resultatTexte').append(json[data[0].user.couleur1].seul)
        $('.resultatTexte').append(
          json[data[0].user.couleur1][data[0].user.couleur2]
        )
      })
    }
  })
})

function drawRes(res) {
  var chart = new CanvasJS.Chart('chartContainer', {
    animationEnabled: true,
    title: {
      text: 'Vos résultats'
    },
    data: [
      {
        type: 'pie',
        indexLabelFontSize: 18,
        radius: 2000,
        startAngle: 240,
        yValueFormatString: '##0.00"%"',
        indexLabel: '{label} {y}',
        dataPoints: [
          { y: (res.bleu * 100) / 4, label: 'Développeur' },
          { y: (res.rouge * 100) / 4, label: 'Entrepreneur' },
          { y: (res.jaune * 100) / 4, label: 'Chef de projet digital' },
          { y: (res.vert * 100) / 4, label: 'Graphiste' }
        ]
      }
    ]
  })
  chart.render()
}
