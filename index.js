const express = require('express')
const nodemailer = require('nodemailer')
const app = express()
app.use(express.static('web'))

const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    user: process.env.MAIL,
    pass: process.env.PASS
  },
  tls: {
    rejectUnauthorized: false
  }
})

app.get('/', (req, res) => {
  res.sendFile('web/html/index.html', { root: __dirname })
})

app.get('/sendMail', (req, res) => {
  var message = {
    from: 'susu54650@gmail.com',
    to: 'juline_palenzuela@hotmail.fr',
    subject: "J'arrive à envoyer des mails youpi",
    text: 'Salut chat',
    html: '<p><b>Salut mais en gras</b></p>'
  }

  transporter.sendMail(message, (error, info) => {
    if (error) {
      return console.log(error)
      res.send('Message not send')
    }
    console.log('Message sent: %s', info.messageId)
    res.send('Message send')
  })
})

app.listen(3000, () => {
  console.log('Example app listening on port 3000!')
})
